<?php

require_once 'Type.php';

class Food extends Type {
	public $name;
	public $price;
	public $rating;

	public function __construct($name, $price, $rating = '4.9/5') {
		$this->type = 'Food';
		$this->name = $name;
		$this->price = $price;
		$this->rating = $rating;
	}

	public function information() {
		echo 'Below is your ' . $this->type . ' information:<br>';
		echo 'Food Name &nbsp&nbsp: ' . $this->name . '<br>';
		echo 'Food Price &nbsp&nbsp&nbsp: ' . $this->price . '<br>';
		echo 'Food Rating &nbsp: ' . $this->rating . '<br>';
	}
}

?>
<!-- tidak ada tag penutup php -->
<!-- saya ubah protected jadi public biar bisa di akses di luar class /turunan nya -->